
//  SWRecordViewController2.swift
//  SWVideoRecorder
//
//  Created by CHOI-MS on 2016. 11. 16..
//  Copyright © 2016년 CHOI-MS. All rights reserved.
//

import UIKit
import AVFoundation

class SWRecordViewController2: UIViewController, RecordSessionDelegate, RecordAssetWriterDelegate {

    @IBOutlet var previewView: SWRecordPreviewView!
    
    private let player = AVPlayer()
    private var backgroundRecordingID: UIBackgroundTaskIdentifier? = nil
    private var sessionCoordinator: SWRecordSessionCoordinator!
    private var assetWriterCoordinator: SWRecordAssetWritingCoordinator!
    private var options: SWRecordOptions = [.audio, .video]
//    private var options: SWRecordOptions = [.audio]
    private var videoSourceFormatDescription: CMFormatDescription? {
        didSet {
            if self.videoSourceFormatDescription != nil {
                self.videoCompressionSettings = self.getVideoCompressionSettings(width: previewView.bounds.width, height: previewView.bounds.height)
            }
        }
    }
    
    private func getVideoCompressionSettings(width: CGFloat, height: CGFloat) -> [String : Any] {
        return [AVVideoCodecKey : AVVideoCodecH264,
                AVVideoWidthKey : width,
                AVVideoHeightKey : height,
                AVVideoScalingModeKey : AVVideoScalingModeResizeAspectFill]
    }
    
    private var videoCompressionSettings: [String:Any]? {
        didSet {
            if self.videoSourceFormatDescription != nil && self.videoCompressionSettings != nil {
                assetWriterCoordinator.setVideoTrackHint(self.videoSourceFormatDescription!, settings: self.videoCompressionSettings!)
            }
        }
    }
    
    private var audioSourceFormatDescription: CMFormatDescription? {
        didSet {
            if self.audioSourceFormatDescription != nil {
                let audioCompressionSettings: [String:Any] = [AVFormatIDKey : kAudioFormatMPEG4AAC]
                assetWriterCoordinator.setAudioTrackHint(self.audioSourceFormatDescription, settings: audioCompressionSettings)
            }
        }
    }
    
    private var isSessionReady = false
    private var playerContext = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        let fileURL = Bundle.main.url(forResource: "bigbang", withExtension: "m4a")
//        let fileURL = Bundle.main.url(forResource: "palza", withExtension: "mp3")
//        let fileURL = Bundle.main.url(forResource: "panicstation", withExtension: "mp3")
        let fileURL = Bundle.main.url(forResource: "bernadettesong", withExtension: "m4a")
        let playerItem = AVPlayerItem(url: fileURL!)
        player.replaceCurrentItem(with: playerItem)
        player.addObserver(self, forKeyPath: "rate", options: [.new], context: &playerContext)
        
        sessionCoordinator = SWRecordSessionCoordinator(delegate: self)
        assetWriterCoordinator = SWRecordAssetWritingCoordinator(options: options, delegate: self)
        
        previewView.session = sessionCoordinator.captureSession
        previewView.videoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        
        sessionCoordinator.configureDeviceInputs(options: options, completion: { [unowned self] (configureResult) in
            print("result = \(configureResult)")
            
            if configureResult == true {
                self.sessionCoordinator.configureOutputsAndConnections(options: self.options, completion: { (outputConfigureResult) in
                    print("configureOutputsAndConnections result = \(outputConfigureResult)")
                    self.videoCompressionSettings = self.getVideoCompressionSettings(width: self.previewView.bounds.size.width, height: self.previewView.bounds.size.height)
                    self.sessionCoordinator.setVideoConnectionOrientation(.portrait)
                    
                    self.sessionCoordinator.captureSession.startRunning()
                    self.isSessionReady = true
                })
            }
        })
    }
    
    private func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(sessionWasInterrupted), name: Notification.Name("AVCaptureSessionWasInterruptedNotification"), object: sessionCoordinator.captureSession)
        NotificationCenter.default.addObserver(self, selector: #selector(sessionInterruptionEnded), name: Notification.Name("AVCaptureSessionInterruptionEndedNotification"), object: sessionCoordinator.captureSession)
    }
    
    private func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }
    
    func sessionWasInterrupted(notification: NSNotification) {
        print("was interrupted!!") // 이때 레코딩 종료하면 되겠지
        
        if self.assetWriterCoordinator.isRecording {
            self.stopRecording()
        }
        /*
         In some scenarios we want to enable the user to resume the session running.
         For example, if music playback is initiated via control center while
         using AVCam, then the user can let AVCam resume
         the session running, which will stop music playback. Note that stopping
         music playback in control center will not automatically resume the session
         running. Also note that it is not always possible to resume, see `resumeInterruptedSession(_:)`.
         */
        if let userInfoValue = notification.userInfo?[AVCaptureSessionInterruptionReasonKey] as AnyObject?, let reasonIntegerValue = userInfoValue.integerValue, let reason = AVCaptureSessionInterruptionReason(rawValue: reasonIntegerValue) {
            print("Capture session was interrupted with reason \(reason)")
            
            var showResumeButton = false
            
            if reason == AVCaptureSessionInterruptionReason.audioDeviceInUseByAnotherClient || reason == AVCaptureSessionInterruptionReason.videoDeviceInUseByAnotherClient {
                showResumeButton = true
            }
            else if reason == AVCaptureSessionInterruptionReason.videoDeviceNotAvailableWithMultipleForegroundApps {
                // Simply fade-in a label to inform the user that the camera is unavailable.
//                cameraUnavailableLabel.alpha = 0
//                cameraUnavailableLabel.isHidden = false
//                UIView.animate(withDuration: 0.25) { [unowned self] in
//                    self.cameraUnavailableLabel.alpha = 1
//                }
                print("videoDeviceNotAvailableWithMultipleForegroundApps")
            }
            
            if showResumeButton {
                // Simply fade-in a button to enable the user to try to resume the session running.
//                resumeButton.alpha = 0
//                resumeButton.isHidden = false
//                UIView.animate(withDuration: 0.25) { [unowned self] in
//                    self.resumeButton.alpha = 1
//                }
                print("showResumeButton")
            }
        }
    }
    
    func sessionInterruptionEnded(notification: NSNotification) {
        print("end interrupted!!") // 이때 화면 이동하면 되겠지
    }
    
    override func viewWillAppear(_ animated: Bool) {
        addObservers()
        super.viewWillAppear(animated)
        
        if isSessionReady == true {
            sessionCoordinator.captureSession.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        removeObservers()
        super.viewWillDisappear(animated)
        
        if sessionCoordinator.captureSession.isRunning == true {
            sessionCoordinator.captureSession.stopRunning()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - Orientation
    override var shouldAutorotate: Bool {
        // Disable autorotation of the interface when recording is in progress.
        return !assetWriterCoordinator.isRecording
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .all
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        print("size = \(size)")
        
        if let videoPreviewLayerConnection = previewView.videoPreviewLayer.connection {
            let deviceOrientation = UIDevice.current.orientation
            guard let newVideoOrientation = deviceOrientation.videoOrientation, deviceOrientation.isPortrait || deviceOrientation.isLandscape else {
                return
            }
            
            videoPreviewLayerConnection.videoOrientation = newVideoOrientation
            videoCompressionSettings = getVideoCompressionSettings(width: size.width, height: size.height) // 이 크기를 나중에 조정해야한다. 480 베이스에 맞추어서
            sessionCoordinator.setVideoConnectionOrientation(newVideoOrientation)
        }
    }
    
    
    // MARK: - Recording
    func startRecording() {
        if self.assetWriterCoordinator.isRecording == false {
            activateBackgroundMode()
            
            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let copiedPath = documentsPath.appending("/copiedVideo.mp4")
            
            let pathURL = URL(fileURLWithPath: copiedPath)
            
//            videoCompressionSettings = getVideoCompressionSettings(width: previewView.bounds.size.width, height: previewView.bounds.size.height)
//            sessionCoordinator.setVideoConnectionOrientation(previewView.videoPreviewLayer.connection.videoOrientation)
            if self.assetWriterCoordinator.prepareRecording(at: pathURL) {
//                let inputMasterClock = sessionCoordinator.getInputMasterClock()
//                
//                player.masterClock = inputMasterClock
                
                self.assetWriterCoordinator.startRecording(at: pathURL)
            }
        }
    }
    
    func stopRecording() {
        if self.assetWriterCoordinator.isRecording == true {
            self.assetWriterCoordinator.finishRecording()
        }
    }
    
    // MARK: - SWRecordSessionCoordinator Delegate
    func sampleBufferCaptured(sampleBuffer: CMSampleBuffer!, mediaType: String) {
        
        if mediaType == AVMediaTypeVideo {
            if videoSourceFormatDescription == nil {
                videoSourceFormatDescription = CMSampleBufferGetFormatDescription(sampleBuffer)
            }
        } else if mediaType == AVMediaTypeAudio {
            if audioSourceFormatDescription == nil {
                audioSourceFormatDescription = CMSampleBufferGetFormatDescription(sampleBuffer)
            }
        }
        
        self.assetWriterCoordinator.appendSampleBuffer(sampleBuffer, mediaType: mediaType)
    }
    
    // MARK: - IBAction
    
    @IBAction func switchButtonPressed(_ sender: UIButton) {
        if self.assetWriterCoordinator.isRecording == false {
            sender.isSelected = !sender.isSelected
            
            var cameraPosition: AVCaptureDevicePosition
            if sender.isSelected {
                cameraPosition = .back
            } else {
                cameraPosition = .front
            }
            
            UIView.animate(withDuration: 0.3, delay: 0.0, options: [.beginFromCurrentState], animations: {
                self.previewView.transform = CGAffineTransform.init(scaleX: -1, y: 1)
            }, completion: { (result) in
                self.previewView.transform = .identity
            })
            
            sessionCoordinator.captureSession.stopRunning()
            sessionCoordinator.switchCamera(position: cameraPosition, completion: { [unowned self] (switchResult) in
                if switchResult == true {
                    self.sessionCoordinator.captureSession.startRunning()
                }
            })
        }
    }
    let masterClock = CMClockGetHostTimeClock()
    @IBAction func recordButtonPressed(_ sender: UIButton) {
        if self.assetWriterCoordinator.isRecording == true {
            sender.isSelected = false
            stopRecording()
            player.pause()
            player.seek(to: kCMTimeZero)
        } else {
            sender.isSelected = true
            
            let startPress = CMClockGetTime(masterClock)
            self.startPressTime = startPress
            player.play()
            startRecording()
        }
    }
    
    // MARK: - focus
    @IBAction private func focusAndExposeTap(_ gestureRecognizer: UITapGestureRecognizer) {
        let devicePoint = self.previewView.videoPreviewLayer.captureDevicePointOfInterest(for: gestureRecognizer.location(in: gestureRecognizer.view))
        self.sessionCoordinator.focus(with: .autoFocus, exposureMode: .autoExpose, at: devicePoint, monitorSubjectAreaChange: true)
    }
    
    // MARK: - Control BackgroundID
    private func activateBackgroundMode() {
        if UIDevice.current.isMultitaskingSupported {
            /*
             Setup background task.
             This is needed because the `capture(_:, didFinishRecordingToOutputFileAt:, fromConnections:, error:)`
             callback is not received until AVCam returns to the foreground unless you request background execution time.
             This also ensures that there will be time to write the file to the photo library when AVCam is backgrounded.
             To conclude this background execution, endBackgroundTask(_:) is called in
             `capture(_:, didFinishRecordingToOutputFileAt:, fromConnections:, error:)` after the recorded file has been saved.
             */
            self.backgroundRecordingID = UIApplication.shared.beginBackgroundTask(expirationHandler: nil)
        }
    }
    
    private func deactivateBackgroundMode() {
        if let currentBackgroundRecordingID = backgroundRecordingID {
            backgroundRecordingID = UIBackgroundTaskInvalid
            
            if currentBackgroundRecordingID != UIBackgroundTaskInvalid {
                UIApplication.shared.endBackgroundTask(currentBackgroundRecordingID)
            }
        }
    }
    
    var startPressTime = kCMTimeZero
    var willStartTime = kCMTimeZero
    var willWriteTime = kCMTimeZero
    var didStartTime = kCMTimeZero
    var difference = kCMTimeZero
    
    let player2 = AVPlayer()
    
    // MARK: - SWAssetWriterCoordinatorDelegate
    func recordAssetWriterWillStartRecording(_ recordAssetWriter: SWRecordAssetWritingCoordinator) {
        print("recordAssetWriterWillStartRecording")
        willStartTime = CMClockGetTime(masterClock)
    }
    
    func recordAssetWriterWillWriteRecording(_ recordAssetWriter: SWRecordAssetWritingCoordinator) {
        print("recordAssetWriterWillWriteRecording")
        willWriteTime = CMClockGetTime(masterClock)
    }
    func recordAssetWriterDidStartRecording(_ recordAssetWriter: SWRecordAssetWritingCoordinator) {
        print("recordAssetWriterDidStartRecording")
        didStartTime = CMClockGetTime(masterClock)
        
////        let difference = CMTimeSubtract(didStartTime, willStartTime) // 전혀 안맞음. 유저 목소리가 더 빠르다
////        let difference = CMTimeSubtract(didStartTime, willWriteTime) // 전혀 안맞음. 유저 목소리가 더 느리다 이건 살짝 느리다. willWrite와 willStart 의 gap 을 구해야하네. 그럼 will write 에서 will start 를 뺀거를 구하고(1) didStart 에서 will write 빼고 (1)을 더 빼주자
//        let differenceBetweenWillWriteAndWillStart = CMTimeSubtract(willWriteTime, willStartTime) // assetWriter 의 startWriting 과 첫번째 버퍼가 저장될때의 gap
//        let differenceBetweenDidStartAndWillWrite = CMTimeSubtract(didStartTime, willWriteTime) // assetWriter의 첫번째 버퍼가 저장될 때와 저장된 후의 gap
//        self.difference = CMTimeSubtract(differenceBetweenDidStartAndWillWrite, startPressTime) // 이게 좀 맞는것 같음.
        
        // gap1 : willStart - didPress
        // gap2 : willWrite - willStart
        // gap3 : didStart - willWrite
        let gap1 = CMTimeSubtract(willStartTime, startPressTime)
        let gap2 = CMTimeSubtract(willWriteTime, willStartTime)
        let gap3 = CMTimeSubtract(didStartTime, willWriteTime)
        
        let allGaps = CMTimeAdd(CMTimeAdd(gap1, gap2), gap3)
        
        print("gap1 = \(CMTimeGetSeconds(gap1))")
        print("gap2 = \(CMTimeGetSeconds(gap2))")
        print("gap3 = \(CMTimeGetSeconds(gap3))")
        print("allGaps Seconds = \(CMTimeGetSeconds(allGaps)))")
        
        print("rate gap = \(CMTimeGetSeconds(playerRateDelayTime))")
        let endToEndGap = CMTimeSubtract(didStartTime, startPressTime)
//        let difference = CMTimeSubtract(endToEndGap, gap2) // 이거도 좀 맞는거 같음 (5, 10 에서 잘맞음)
//        let difference = CMTimeSubtract(endToEndGap, gap2), gap3) // 이게 제일 잘맞는듯 (내폰에서)
//        let difference = CMTimeAdd(CMTimeSubtract(CMTimeSubtract(endToEndGap, gap2), gap3), playerRateDelayTime) // 이게 제일 잘맞는듯 (내폰에서) (5, 10 에서도 잘맞는거같음)
//        let difference = CMTimeAdd(gap1, playerRateDelayTime) // 이게 바로 위의 계산이랑 같지..
        let difference = CMTimeSubtract(gap1, playerRateDelayTime) // 이게 맞는 계산인것 같다. 스타트 버튼 눌렀을때부터 녹음될 시간차에서 플레이어의 플레이 딜레이 타임을 뺀다.
        print("endToEnd gap Seconds = \(CMTimeGetSeconds(endToEndGap))")
        self.difference = difference
        
        print("difference = \(CMTimeGetSeconds(self.difference))")
    }
    func recordAssetWriterDidFinshRecording(_ recordAssetWriter: SWRecordAssetWritingCoordinator) {
        print("recordAssetWriterDidFinshRecording")
        deactivateBackgroundMode()
        
        let item = AVPlayerItem(url: recordAssetWriter.outputURL!)
        player2.replaceCurrentItem(with: item)
        
        player2.seek(to: difference)
        player2.volume = 10.0
    }
    func recordAssetWriterDidCancelRecording(_ recordAssetWriter: SWRecordAssetWritingCoordinator) {
        print("recordAssetWriterDidCancelRecording")
        deactivateBackgroundMode()
    }
    func recordAssetWriterDidFailRecording(_ recordAssetWriter: SWRecordAssetWritingCoordinator) {
        print("recordAssetWriterDidFailRecording")
        deactivateBackgroundMode()
    }
    
    @IBAction func playbuttonPressed(_ sender: UIButton) {
        player.play()
        player2.play()
    }
    
    @IBAction func stopPlaying(_ sender: UIButton)
    {
        player.pause()
        player2.pause()
        player.seek(to: kCMTimeZero)
        player2.seek(to: difference)
    }
    
    // MARK: - AVPlayer KVO
    var playerRateDelayTime = kCMTimeZero
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard &playerContext == context else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
            return
        }
        
        switch keyPath! {
        case "rate":
            if player.rate != 0 {
                let playedTime = CMClockGetTime(masterClock)
                playerRateDelayTime = CMTimeSubtract(playedTime, startPressTime)
            } else {
                playerRateDelayTime = kCMTimeZero
            }
        default: break;
        }
    }
}
