//
//  SWRecordInterface.swift
//  SWVideoRecorder
//
//  Created by CHOI-MS on 2016. 11. 16..
//  Copyright © 2016년 CHOI-MS. All rights reserved.
//

import Foundation

struct SWRecordOptions: OptionSet {
    let rawValue: Int
    
    static let audio = SWRecordOptions(rawValue: 1 << 0) // only sound
    static let video = SWRecordOptions(rawValue: 1 << 1) // only screen
}
