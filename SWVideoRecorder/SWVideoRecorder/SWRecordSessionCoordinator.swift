//
//  SWRecordSessionCoordinator.swift
//  SWVideoRecorder
//
//  Created by CHOI-MS on 2016. 11. 16..
//  Copyright © 2016년 CHOI-MS. All rights reserved.
//

import Foundation
import AVFoundation

class SWRecordSessionCoordinator: NSObject, AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureAudioDataOutputSampleBufferDelegate {
    
    // MARK: - Queues
    private let sessionQueue = DispatchQueue(label: "session queue", attributes: [], target: nil) // Communicate with the session and other session objects on this queue.
    private let videoDataOutputQueue = DispatchQueue(label: "videoOutput queue", attributes: [], target: nil)
    private let audioDataOutputQueue = DispatchQueue(label: "audioOutput queue", attributes: [], target: nil)
    
    // MARK: - Session
    let captureSession = AVCaptureSession()
    
    // MARK: - DelegationObject
    private weak var delegate: RecordSessionDelegate!
    
    // MARK: - DeviceInputs
    private var videoDeviceInput: AVCaptureDeviceInput? {
        didSet {
            removeInput(oldValue)
        }
    }
    private var audioDeviceInput: AVCaptureDeviceInput? {
        didSet {
            removeInput(oldValue)
        }
    }
    
    // MARK: - DataOutputs
    private var videoDataOutput: AVCaptureVideoDataOutput?
    private var audioDataOutput: AVCaptureAudioDataOutput?
    
    // MARK: - Connections
    private var videoConnection: AVCaptureConnection?
    private var audioConnection: AVCaptureConnection?
    
    // MARK: - Initializer
    init(delegate: RecordSessionDelegate) {
        self.delegate = delegate
    }
    
    // MARK: - Controls for Input Devices
    func configureDeviceInputs(options: SWRecordOptions, completion: @escaping (_: Bool)->Swift.Void) {
        if options.contains(.audio) {
            configureAudioInputDevice(completion: { [unowned self](audioConfigureResult) in
                if options.contains(.video) {
                    self.configureVideoInputDevice(completion: { (videoConfigureResult) in
                        completion(audioConfigureResult && videoConfigureResult)
                    })
                } else {
                    completion(audioConfigureResult)
                }
            })
        }
    }
    
    private func removeInput(_ input: AVCaptureInput?) {
        guard let removeInput = input else { return }
        captureSession.removeInput(removeInput)
    }
    
    private func configureVideoInputDevice(completion: @escaping (_: Bool)->Swift.Void) {
        sessionQueue.async { [unowned self] in
            var configurationSuccess = false
            self.captureSession.beginConfiguration()
            
            self.captureSession.sessionPreset = AVCaptureSessionPresetHigh
            
            do {
                var defaultVideoDevice: AVCaptureDevice?
                
                defaultVideoDevice = self.getCameraDevice(position: .front)
                
                let videoDeviceInput = try AVCaptureDeviceInput(device: defaultVideoDevice)
                
                if self.captureSession.canAddInput(videoDeviceInput) {
                    self.captureSession.addInput(videoDeviceInput)
                    self.videoDeviceInput = videoDeviceInput
                    configurationSuccess = true
                }
                else {
                    print("Could not add video device input to the session")
                }
            }
            catch {
                print("Could not create video device input: \(error)")
            }
            
            self.captureSession.commitConfiguration()
            completion(configurationSuccess)
        }
    }
    
    private func configureAudioInputDevice(completion: @escaping (_: Bool)->Swift.Void) {
        sessionQueue.async { [unowned self] in
            var configurationSuccess = false
            self.captureSession.beginConfiguration()
            do {
                let audioDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeAudio)
                let audioDeviceInput = try AVCaptureDeviceInput(device: audioDevice)
                
                if self.captureSession.canAddInput(audioDeviceInput) {
                    self.captureSession.addInput(audioDeviceInput)
                    self.audioDeviceInput = audioDeviceInput
                    configurationSuccess = true
                }
                else {
                    print("Could not add audio device input to the session")
                }
            }
            catch {
                print("Could not create audio device input: \(error)")
            }
            self.captureSession.commitConfiguration()
            completion(configurationSuccess)
        }
    }
    
    // MARK: - Configuration DataOutputs and Connections
    func configureOutputsAndConnections(options: SWRecordOptions, completion: @escaping(_: Bool)->Swift.Void) {
        if options.contains(.audio) {
            configureAudioOutputAndConnection(completion: { [unowned self] (audioConfigurationResult) in
                if audioConfigurationResult == true {
                    if options.contains(.video) {
                        self.configureVideoOutputAndConnection(completion: { (videoConfigurationResult) in
                            completion(videoConfigurationResult)
                            return
                        })
                    } else {
                        completion(audioConfigurationResult)
                        return
                    }
                } else {
                    completion(audioConfigurationResult)
                    return
                }
            })
        } else if options.contains(.video) {
            configureVideoOutputAndConnection(completion: completion)
        }
    }
    
    private func configureVideoOutputAndConnection(completion: @escaping(_: Bool)->Swift.Void) {
            configureVideoDataOutput(completion: { [unowned self] (configureDataOutputResult) in
            if configureDataOutputResult == true {
                self.videoConnection = self.videoDataOutput!.connection(withMediaType: AVMediaTypeVideo)
            }
            completion(configureDataOutputResult)
        })
    }
    
    private func configureAudioOutputAndConnection(completion: @escaping(_: Bool)->Swift.Void) {
        configureAudioDataOutput(completion: { [unowned self] (configureDataOutputResult) in
            if configureDataOutputResult == true {
                self.audioConnection = self.audioDataOutput!.connection(withMediaType: AVMediaTypeAudio)
            }
            completion(configureDataOutputResult)
        })
    }
    
    // MARK: - Configuration DataOutput
    private func configureVideoDataOutput(completion: @escaping(_: Bool)->Swift.Void) {
            if self.videoDataOutput != nil {
                self.captureSession.removeOutput(self.videoDataOutput!)
            }
            self.videoDataOutput = AVCaptureVideoDataOutput()
            self.videoDataOutput!.alwaysDiscardsLateVideoFrames = false
            self.videoDataOutput!.setSampleBufferDelegate(self, queue: self.videoDataOutputQueue)
            
            if self.captureSession.canAddOutput(self.videoDataOutput!) {
                self.captureSession.addOutput(self.videoDataOutput!)
                completion(true)
                return
            }
            completion(false)
    }
    
    private func configureAudioDataOutput(completion: @escaping(_: Bool)->Swift.Void) {
            if self.audioDataOutput != nil {
                self.captureSession.removeOutput(self.audioDataOutput!)
            }
            
            self.audioDataOutput = AVCaptureAudioDataOutput()
            self.audioDataOutput!.setSampleBufferDelegate(self, queue: self.audioDataOutputQueue)
            
            if self.captureSession.canAddOutput(self.audioDataOutput) {
                self.captureSession.addOutput(self.audioDataOutput)
                completion(true)
                return
            }
            
            completion(false)
    }
    
    // MARK: - Configuration Connection
    func setVideoConnectionOrientation(_ orientation: AVCaptureVideoOrientation) {
        if self.videoConnection?.isVideoOrientationSupported == true {
            self.videoConnection?.videoOrientation = orientation
        }
    }
    
    // MARK: - Get Device
    private func getCameraDevice(position: AVCaptureDevicePosition) -> AVCaptureDevice? {
        
        if #available(iOS 10.0, *) {
            var defaultVideoDevice: AVCaptureDevice?
            switch position {
            case .front:
                if let frontCameraDevice = AVCaptureDevice.defaultDevice(withDeviceType: .builtInWideAngleCamera, mediaType: AVMediaTypeVideo, position: .front) {
                    // In some cases where users break their phones, the back wide angle camera is not available. In this case, we should default to the front wide angle camera.
                    defaultVideoDevice = frontCameraDevice
                }
            case .back:
                if let backCameraDevice = AVCaptureDevice.defaultDevice(withDeviceType: .builtInWideAngleCamera, mediaType: AVMediaTypeVideo, position: .back) {
                    // If the back dual camera is not available, default to the back wide angle camera.
                    defaultVideoDevice = backCameraDevice
                }
            default:
                break;
            }
            return defaultVideoDevice
        } else {
            guard let devices = AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo) as? [AVCaptureDevice] else { return nil }
            
            for device in devices {
                if device.position == position {
                    return device
                }
            }
            return nil
        }
    }
    
    func switchCamera(position: AVCaptureDevicePosition, completion: @escaping (_: Bool) -> Swift.Void) {
        guard  let currentVideoInput = videoDeviceInput else { return }
        
        sessionQueue.async { [unowned self] in
            var newVideoInput: AVCaptureDevice?
            switch currentVideoInput.device.position {
            case .front:
                newVideoInput = self.getCameraDevice(position: .back)
            default:
                newVideoInput = self.getCameraDevice(position: .front)
            }
            
            if newVideoInput != nil {
                do {
                    let videoDeviceInput = try AVCaptureDeviceInput(device: newVideoInput)
                    
                    self.captureSession.beginConfiguration()
                    
                    // Remove the existing device input first, since using the front and back camera simultaneously is not supported.
                    self.captureSession.removeInput(self.videoDeviceInput)
                    
                    if self.captureSession.canAddInput(videoDeviceInput) {
                        
                        self.captureSession.addInput(videoDeviceInput)
                        self.videoDeviceInput = videoDeviceInput
                        self.captureSession.commitConfiguration()
                        
                        let previousVideoConnection = self.videoConnection
                        self.videoConnection = self.videoDataOutput?.connection(withMediaType: AVMediaTypeVideo) // 카메라 변경 후, 커넥션을 다시 연결해준다.
                        
                        self.updateVideoConnection(from: previousVideoConnection)
                        completion(true)
                        return
                    }
                    else {
                        print("Can Not Switch Camera")
                        self.captureSession.addInput(self.videoDeviceInput);
                        self.captureSession.commitConfiguration()
                        completion(false)
                        return
                    }
                    
                }
                catch {
                    print("Error occured while creating video device input: \(error)")
                    completion(false)
                    return
                }
            } else {
                print("can Not Get New Input")
                completion(false)
            }
        }
    }
    
    func updateVideoConnection(from previousConnection:  AVCaptureConnection?) {
        // 원래 videoConnection 의 didSet 에 넣었는데 딜레이가 커서 수정함.
        if let oldConnection = previousConnection {
            if let orientationSupported = videoConnection?.isVideoOrientationSupported {
                if orientationSupported {
                    videoConnection?.videoOrientation = oldConnection.videoOrientation
                }
            }
            
            if let videoStabilizationSupported = self.videoConnection?.isVideoStabilizationSupported {
                if videoStabilizationSupported {
                    self.videoConnection?.preferredVideoStabilizationMode = oldConnection.preferredVideoStabilizationMode
                }
            }
        }
    }
    
    // MARK: - Focus
    func focus(with focusMode: AVCaptureFocusMode, exposureMode: AVCaptureExposureMode, at devicePoint: CGPoint, monitorSubjectAreaChange: Bool) {
        sessionQueue.async { [unowned self] in
            if let device = self.videoDeviceInput?.device {
                do {
                    try device.lockForConfiguration()
                    
                    /*
                     Setting (focus/exposure)PointOfInterest alone does not initiate a (focus/exposure) operation.
                     Call set(Focus/Exposure)Mode() to apply the new point of interest.
                     */
                    if device.isFocusPointOfInterestSupported && device.isFocusModeSupported(focusMode) {
                        device.focusPointOfInterest = devicePoint
                        device.focusMode = focusMode
                    }
                    
                    if device.isExposurePointOfInterestSupported && device.isExposureModeSupported(exposureMode) {
                        device.exposurePointOfInterest = devicePoint
                        device.exposureMode = exposureMode
                    }
                    
                    device.isSubjectAreaChangeMonitoringEnabled = monitorSubjectAreaChange
                    device.unlockForConfiguration()
                }
                catch {
                    print("Failed to set Focus. Could not lock device for configuration: \(error)")
                }
            }
        }
    }
    
    // MARK: - DataOutputSampleBufferDelegate
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, from connection: AVCaptureConnection!) {
        /*
        let port = connection.inputPorts[0] as! AVCaptureInputPort
        let orginalClock = port.clock
        let syncedPTS = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)
        if let oriClock = orginalClock {
            let originalPTS = CMSyncConvertTime(syncedPTS, captureSession.masterClock, oriClock)
        }
 */
        if connection == videoConnection {
            delegate.sampleBufferCaptured(sampleBuffer: sampleBuffer, mediaType: AVMediaTypeVideo)
        } else if connection == audioConnection {
            delegate.sampleBufferCaptured(sampleBuffer: sampleBuffer, mediaType: AVMediaTypeAudio)
        }
    }
    
    // MARK: - test
    func getInputMasterClock() -> CMClock? {
//        if let audioInputPort = audioConnection?.inputPorts[0] as? AVCaptureInputPort {
//            return audioInputPort.clock
//        } else
            if let videoInputPort = videoConnection?.inputPorts[0] as? AVCaptureInputPort {
            return videoInputPort.clock
        } else if let audioInputPort = audioConnection?.inputPorts[0] as? AVCaptureInputPort {
                return audioInputPort.clock
            }else {
            return nil
        }
    }
}

protocol RecordSessionDelegate: class {
    func sampleBufferCaptured(sampleBuffer: CMSampleBuffer!, mediaType: String)
}
