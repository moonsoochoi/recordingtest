//
//  SWRecordAssetWritingCoordinator.swift
//  SWVideoRecorder
//
//  Created by CHOI-MS on 2016. 11. 16..
//  Copyright © 2016년 CHOI-MS. All rights reserved.
//

/*
 AVAssetWriter 를 담당한다.
 */

import Foundation
import AVFoundation
import Photos

class SWRecordAssetWritingCoordinator {
    
    // MARK: - Delegate
    private weak var delegate: RecordAssetWriterDelegate?
    
    // MARK: - Flag for Recording
    private (set) var isRecording = false
    
    // MARK: - Initializer
    init(options: SWRecordOptions, delegate: RecordAssetWriterDelegate?) {
        self.recordOptions = options
        self.delegate = delegate
        
        if recordOptions.contains(.video) {
            outputFileType = AVFileTypeMPEG4
        } else {
            outputFileType = AVFileTypeAppleM4A
        }
    }
    
    // MARK: - Recording API
    func startRecording(at outputURL: URL) {
        self.outputURL = outputURL
        writingQueue.async { [unowned self] in
            if self.isRecording == false  {
                self.isRecording = true
            }
        }
    }
    
    func finishRecording() {
        writingQueue.async { [unowned self] in
            if self.isRecording == true {
                self.isRecording = false // finishWriting 이 완료 된 시점에 이 값을 바꾸게 되면, finishWriting 이 완료되기 전에 버퍼에 값이 들어와서 exception 발생된다.
                self.isFirstSample = true
                self.assetWriter?.finishWriting { [unowned self] in
                    self.copyFileToCameraRoll()
                    
                    print("time to call Delegate. status = \(self.assetWriter!.status.rawValue)")
                    
                    switch self.assetWriter!.status {
                    case .completed:
                        self.delegate?.recordAssetWriterDidFinshRecording(self)
                    default:
                        self.delegate?.recordAssetWriterDidFailRecording(self)
                        
                    }
                }
            } else {
                print("Can Not Finish Recording. assetWriterStatus = \(self.assetWriter?.status.rawValue)")
                self.delegate?.recordAssetWriterDidFailRecording(self)
            }
        }
    }
    
    // MARK: - Set Hints
    func setVideoTrackHint(_ hint: CMFormatDescription?, settings: [String : Any]?) {
        if self.recordOptions.contains(.video) {
            videoTrackHint = hint
            videoOutputCompressionSetting = settings
        } else {
            videoTrackHint = nil
            videoOutputCompressionSetting = nil
        }
    }
    
    func setAudioTrackHint(_ hint: CMFormatDescription?, settings: [String : Any]?) {
        if self.recordOptions.contains(.audio) {
            audioTrackHint = hint
            audioOutputCompressionSetting = settings
        } else {
            audioTrackHint = nil
            audioOutputCompressionSetting = nil
        }
    }
    
    // MARK: - Add SampleBuffer
    func appendSampleBuffer(_ sampleBuffer: CMSampleBuffer!, mediaType: String) {
        writingQueue.async { [unowned self] in
            
            self.startAssetSessionIfNeeded(sampleBuffer: sampleBuffer, mediaType: mediaType)
            
            if self.isRecording == true && self.assetWriter?.status == .writing {
                if mediaType == AVMediaTypeVideo {
                    
                    if let videoAssetInput = self.videoAssetInput {
                        if videoAssetInput.isReadyForMoreMediaData {
                            if videoAssetInput.append(sampleBuffer) == false {
                                print("Fail to append SampleBuffer to Video")
                            }
                        }
                    }
                } else if mediaType == AVMediaTypeAudio {
                    
                    if let audioAssetInput = self.audioAssetInput {
                        if audioAssetInput.isReadyForMoreMediaData {
                            if audioAssetInput.append(sampleBuffer) == false {
                                print("Fail to append SampleBuffer to Audio")
                            }
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - Queue
    private let writingQueue = DispatchQueue(label: "writing queue", attributes: [], target: nil)
    
    // MARK: - Flag for FirstSample
    private var isFirstSample = true
    
    // MARK: - Output URL
    private (set) var outputURL: URL?
    
    // MARK: - AVAssetWriter
    private var assetWriter: AVAssetWriter? {
        didSet {
            // remove previous assetWriter
            guard let oldAsset = oldValue else { return }
            
            if oldAsset.status == .writing {
                oldAsset.cancelWriting()
                delegate?.recordAssetWriterDidCancelRecording(self)
            }
            
            self.assetWriter?.shouldOptimizeForNetworkUse = true
            self.isRecording = false
            self.isFirstSample = true
        }
    }
    
    // MARK: - AVAssetWriterInputs
    private var videoAssetInput: AVAssetWriterInput? {
        willSet {
            newValue?.expectsMediaDataInRealTime = true
        } didSet {
            addAssetWriteInput(self.videoAssetInput)
        }
    }
    
    private var audioAssetInput: AVAssetWriterInput? {
        willSet {
            newValue?.expectsMediaDataInRealTime = true
        } didSet {
            addAssetWriteInput(self.audioAssetInput)
        }
    }
    
    // MARK: - Record Options
    private var recordOptions: SWRecordOptions = [.audio] {
        didSet {
            if recordOptions.contains(.video) {
                outputFileType = AVFileTypeMPEG4
            } else {
                outputFileType = AVFileTypeAppleM4A
            }
        }
    }
    
    // MARK: - Hints
    private var outputFileType: String = AVFileTypeAppleM4A
    
    private var videoTrackHint: CMFormatDescription?
    private var audioTrackHint: CMFormatDescription?
    
    // MARK: - CompressionSettings
    private var videoOutputCompressionSetting: [String : Any]?
    private var audioOutputCompressionSetting: [String : Any]?
    
    // MARK: - Preparing for Recording
    func prepareRecording(at outputURL: URL) -> Bool {
        try? FileManager.default.removeItem(at: outputURL)
        do {
            assetWriter = try AVAssetWriter(outputURL: outputURL, fileType: outputFileType)
        } catch {
            print("prepareRecording failed")
            return false
        }
        
        prepareAssetInputs()
        return true
    }
    
    private func prepareAssetInputs() {
        prepareVideoInputs()
        prepareAudioInputs()
    }
    
    private func prepareVideoInputs() {
        if recordOptions.contains(.video) {
            videoAssetInput = AVAssetWriterInput(mediaType: AVMediaTypeVideo, outputSettings: videoOutputCompressionSetting, sourceFormatHint: videoTrackHint)
        }
    }
    
    private func prepareAudioInputs() {
        if recordOptions.contains(.audio) {
            audioAssetInput = AVAssetWriterInput(mediaType: AVMediaTypeAudio, outputSettings: audioOutputCompressionSetting, sourceFormatHint: audioTrackHint)
        }
    }
    
    // MARK: - Add Inputs
    private func addAssetWriteInput(_ input: AVAssetWriterInput?) {
        guard let assetInput = input else { return }
        if let canAdd = assetWriter?.canAdd(assetInput) {
            if canAdd {
                assetWriter?.add(assetInput)
            } else {
                print("can NOT add assetWriteInput. type(\(assetInput.mediaType)) !!")
            }
        }
    }
    
    private func startAssetSessionIfNeeded(sampleBuffer: CMSampleBuffer!, mediaType: String) {
        
        if isRecording == true && isFirstSample == true {
            isFirstSample = false
            
            delegate?.recordAssetWriterWillStartRecording(self)
            if let isStartWriting = assetWriter?.startWriting() {
                if isStartWriting == true {
                    print("startRecording!!")
                    let sourceTime = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)
                    
                    if mediaType == AVMediaTypeVideo {
                        if videoTrackHint == nil {
                            videoTrackHint = CMSampleBufferGetFormatDescription(sampleBuffer)
                        }
                    } else if mediaType == AVMediaTypeAudio {
                        if audioTrackHint == nil {
                            audioTrackHint = CMSampleBufferGetFormatDescription(sampleBuffer)
                        }
                    }
//                    delegate?.recordAssetWriterWillStartRecording(self)
                    delegate?.recordAssetWriterWillWriteRecording(self)
                    assetWriter?.startSession(atSourceTime: sourceTime)
                    delegate?.recordAssetWriterDidStartRecording(self)
                } else {
                    print("Can NOT Start Writing")
                    isRecording = false
                    isFirstSample = false
                    delegate?.recordAssetWriterDidFailRecording(self)
                }
            }
        }
    }
    
    // MARK: - Test
    private func copyFileToCameraRoll() {
        
//        PHPhotoLibrary.requestAuthorization {[unowned self]  status in
//            if status == .authorized {
//                // Save the movie file to the photo library and cleanup.
//                PHPhotoLibrary.shared().performChanges({
//                    let options = PHAssetResourceCreationOptions()
//                    options.shouldMoveFile = true
//                    let creationRequest = PHAssetCreationRequest.forAsset()
//                    print("fileURL = \(self.outputURL!)")
//                    creationRequest.addResource(with: .video, fileURL: self.outputURL!, options: options)
//                }, completionHandler: { success, error in
//                    if !success {
//                        print("Could not save movie to photo library: \(error)")
//                    } else {
//                        print("success")
//                    }
//                }
//                )
//            }
//            else {
//                print("photo access fail")
//            }
//        }
    }
}

protocol RecordAssetWriterDelegate: class {
    func recordAssetWriterWillStartRecording(_ recordAssetWriter: SWRecordAssetWritingCoordinator)
    func recordAssetWriterWillWriteRecording(_ recordAssetWriter: SWRecordAssetWritingCoordinator)
    func recordAssetWriterDidStartRecording(_ recordAssetWriter: SWRecordAssetWritingCoordinator)
    func recordAssetWriterDidFinshRecording(_ recordAssetWriter: SWRecordAssetWritingCoordinator)
    func recordAssetWriterDidCancelRecording(_ recordAssetWriter: SWRecordAssetWritingCoordinator)
    func recordAssetWriterDidFailRecording(_ recordAssetWriter: SWRecordAssetWritingCoordinator)
}
