//
//  SWRecordViewController.swift
//  SWVideoRecorder
//
//  Created by CHOI-MS on 2016. 11. 15..
//  Copyright © 2016년 CHOI-MS. All rights reserved.
//

import UIKit
import AVFoundation

class SWRecordViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // Set up the video preview view.
        previewView.session = session
        previewView.videoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill // previewView에 clipsToBounds 해줘야 원하는대로 나온다.
        
        
        /*
         Check video authorization status. Video access is required and audio
         access is optional. If audio access is denied, audio is not recorded
         during movie recording.
         */
        switch AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) {
        case .authorized:
            // The user has previously granted access to the camera.
            break
            
        case .notDetermined:
            /*
             The user has not yet been presented with the option to grant
             video access. We suspend the session queue to delay session
             setup until the access request has completed.
             
             Note that audio access will be implicitly requested when we
             create an AVCaptureDeviceInput for audio during session setup.
             */
            sessionQueue.suspend()
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { [unowned self] granted in
                if !granted {
                    self.setupResult = .notAuthorized
                    print("notAuthorized")
                }
                self.sessionQueue.resume()
            })
            
        default:
            // The user has previously denied access.
            setupResult = .notAuthorized
            print("notAuthorized")
        }
        
        /*
         Setup the capture session.
         In general it is not safe to mutate an AVCaptureSession or any of its
         inputs, outputs, or connections from multiple threads at the same time.
         
         Why not do all of this on the main queue?
         Because AVCaptureSession.startRunning() is a blocking call which can
         take a long time. We dispatch session setup to the sessionQueue so
         that the main queue isn't blocked, which keeps the UI responsive.
         */
        sessionQueue.async { [unowned self] in
            self.configureSession()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        sessionQueue.async {
            switch self.setupResult {
            case .success:
                // Only setup observers and start the session running if setup succeeded.
//                self.addObservers()
                self.session.startRunning()
//                self.isSessionRunning = self.session.isRunning
                
            case .notAuthorized:
                DispatchQueue.main.async { [unowned self] in
                    let message = NSLocalizedString("AVCam doesn't have permission to use the camera, please change privacy settings", comment: "Alert message when the user has denied access to the camera")
                    let alertController = UIAlertController(title: "AVCam", message: message, preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"), style: .cancel, handler: nil))
                    alertController.addAction(UIAlertAction(title: NSLocalizedString("Settings", comment: "Alert button to open Settings"), style: .`default`, handler: { action in
                        UIApplication.shared.openURL(URL(string:UIApplicationOpenSettingsURLString)!)
                    }))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
            case .configurationFailed:
                DispatchQueue.main.async { [unowned self] in
                    let message = NSLocalizedString("Unable to capture media", comment: "Alert message when something goes wrong during capture session configuration")
                    let alertController = UIAlertController(title: "AVCam", message: message, preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"), style: .cancel, handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("hoho")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        sessionQueue.async { [unowned self] in
            if self.setupResult == .success {
                self.session.stopRunning()
//                self.isSessionRunning = self.session.isRunning
//                self.removeObservers()
            }
        }
        
        super.viewWillDisappear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Properties
    private var backgroundRecordingID: UIBackgroundTaskIdentifier? = nil
    private let session = AVCaptureSession()
    @IBOutlet var previewView: SWRecordPreviewView!
    
    private var setupResult: SessionSetupResult = .success
    
    private enum SessionSetupResult {
        case success
        case notAuthorized
        case configurationFailed
    }
    
//    private var isSessionRunning = false
    
    private let sessionQueue = DispatchQueue(label: "session queue", attributes: [], target: nil) // Communicate with the session and other session objects on this queue.
    var videoDeviceInput: AVCaptureDeviceInput?

    private func configureSession() {
//        if setupResult != .success {
//            return
//        }
        
        session.beginConfiguration()
        
        /*
         We do not create an AVCaptureMovieFileOutput when setting up the session because the
         AVCaptureMovieFileOutput does not support movie recording with AVCaptureSessionPresetPhoto.
         */
//        session.sessionPreset = AVCaptureSessionPresetPhoto
        self.session.sessionPreset = AVCaptureSessionPresetHigh // 비디오일 경우 이렇게 세팅. 전체화면으로 나오는데, previewLayer 크기를 조정하고, output 에서 crop 하여 저장해야하기 때문에 assetWriter를 사용해야 한다.
        
        // Add video input.
        do {
            var defaultVideoDevice: AVCaptureDevice?
            
            defaultVideoDevice = getCameraDevice(position: .front)
            
            let videoDeviceInput = try AVCaptureDeviceInput(device: defaultVideoDevice)
            
            if session.canAddInput(videoDeviceInput) {
                session.addInput(videoDeviceInput)
                self.videoDeviceInput = videoDeviceInput
                
                DispatchQueue.main.async {
                    /*
                     Why are we dispatching this to the main queue?
                     Because AVCaptureVideoPreviewLayer is the backing layer for PreviewView and UIView
                     can only be manipulated on the main thread.
                     Note: As an exception to the above rule, it is not necessary to serialize video orientation changes
                     on the AVCaptureVideoPreviewLayer’s connection with other session manipulation.
                     
                     Use the status bar orientation as the initial video orientation. Subsequent orientation changes are
                     handled by CameraViewController.viewWillTransition(to:with:).
                     */
                    let statusBarOrientation = UIApplication.shared.statusBarOrientation
                    var initialVideoOrientation: AVCaptureVideoOrientation = .portrait
                    if statusBarOrientation != .unknown {
                        if let videoOrientation = statusBarOrientation.videoOrientation {
                            initialVideoOrientation = videoOrientation
                        }
                    }
                    
                    self.previewView.videoPreviewLayer.connection.videoOrientation = initialVideoOrientation
                    // mschoi test
//                    self.previewView.frame = CGRect(x: 0, y: 300, width: 375, height: 375)
                    // mschoi test end
                }
            }
            else {
                print("Could not add video device input to the session")
//                setupResult = .configurationFailed
                session.commitConfiguration()
                return
            }
        }
        catch {
            print("Could not create video device input: \(error)")
//            setupResult = .configurationFailed
            session.commitConfiguration()
            return
        }
        
        // Add audio input.
        do {
            let audioDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeAudio)
            let audioDeviceInput = try AVCaptureDeviceInput(device: audioDevice)
            
            if session.canAddInput(audioDeviceInput) {
                session.addInput(audioDeviceInput)
            }
            else {
                print("Could not add audio device input to the session")
            }
        }
        catch {
            print("Could not create audio device input: \(error)")
        }

        
        session.commitConfiguration()
    }
    
    override var shouldAutorotate: Bool {
        // Disable autorotation of the interface when recording is in progress.
//        if let movieFileOutput = movieFileOutput {
//            return !movieFileOutput.isRecording
//        }
        
        return !recording
//        return true
    }
    
    private func getCameraDevice(position: AVCaptureDevicePosition) -> AVCaptureDevice? {
        
        if #available(iOS 10.0, *) {
            var defaultVideoDevice: AVCaptureDevice?
            switch position {
            case .front:
                if let frontCameraDevice = AVCaptureDevice.defaultDevice(withDeviceType: .builtInWideAngleCamera, mediaType: AVMediaTypeVideo, position: .front) {
                    // In some cases where users break their phones, the back wide angle camera is not available. In this case, we should default to the front wide angle camera.
                    defaultVideoDevice = frontCameraDevice
                }
            case .back:
                if let backCameraDevice = AVCaptureDevice.defaultDevice(withDeviceType: .builtInWideAngleCamera, mediaType: AVMediaTypeVideo, position: .back) {
                    // If the back dual camera is not available, default to the back wide angle camera.
                    defaultVideoDevice = backCameraDevice
                }
            default:
                break;
            }
            return defaultVideoDevice
        } else {
                guard let devices = AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo) as? [AVCaptureDevice] else { return nil }
                
                for device in devices {
                    if device.position == position {
                        return device
                    }
                }
                return nil
        }
    }
    
    // MARK: - Orientation
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .all
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        if let videoPreviewLayerConnection = previewView.videoPreviewLayer.connection {
            let deviceOrientation = UIDevice.current.orientation
            guard let newVideoOrientation = deviceOrientation.videoOrientation, deviceOrientation.isPortrait || deviceOrientation.isLandscape else {
                return
            }
            
            videoPreviewLayerConnection.videoOrientation = newVideoOrientation
        }
    }
    
    // MARK: - switch Camera
    
    @IBAction func switchButtonPressed(_ sender: UIButton) {
        guard  let currentVideoInput = videoDeviceInput else { return }
        
        var newVideoInput: AVCaptureDevice?
        switch currentVideoInput.device.position {
        case .front:
            newVideoInput = getCameraDevice(position: .back)
        default:
            newVideoInput = getCameraDevice(position: .front)
        }
        
        if newVideoInput != nil {
            do {
                let videoDeviceInput = try AVCaptureDeviceInput(device: newVideoInput)
                
                self.session.beginConfiguration()
                
                // Remove the existing device input first, since using the front and back camera simultaneously is not supported.
                self.session.removeInput(self.videoDeviceInput)
                
                if self.session.canAddInput(videoDeviceInput) {
//                    NotificationCenter.default.removeObserver(self, name: Notification.Name("AVCaptureDeviceSubjectAreaDidChangeNotification"), object: currentVideoDevice!)
//                    
//                    NotificationCenter.default.addObserver(self, selector: #selector(self.subjectAreaDidChange), name: Notification.Name("AVCaptureDeviceSubjectAreaDidChangeNotification"), object: videoDeviceInput.device)
                    
                    self.session.addInput(videoDeviceInput)
                    self.videoDeviceInput = videoDeviceInput
                }
                else {
                    self.session.addInput(self.videoDeviceInput);
                }
                
//                if let connection = self.movieFileOutput?.connection(withMediaType: AVMediaTypeVideo) {
//                    if connection.isVideoStabilizationSupported {
//                        connection.preferredVideoStabilizationMode = .auto
//                    }
//                }
                
                self.session.commitConfiguration()
            }
            catch {
                print("Error occured while creating video device input: \(error)")
            }
        }
    }
    
    // MARK: - Record
    private var recording = false
    private var fileWriter: SWRecordAssetWriter?
    @IBAction func record(_ sender: UIButton) {
        // 여기서 output을 만들어야 한다.
        // start record 를 할때 output을 시작하고, stop record 일때 output의 completion을 준다.
        recording = !recording
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let copiedPath = documentsPath.appending("/copiedVideo.mp4")
        
        if recording {
                sessionQueue.async { [unowned self] in
                    // prepare output
                    let fileURL = URL(fileURLWithPath: copiedPath)
                    let assetWriter = SWRecordAssetWriter(type: "video", fileURL: fileURL)
                    self.session.beginConfiguration()
                    assetWriter.addOutputs(to: self.session)
                    self.session.commitConfiguration()
                    assetWriter.setVideoSize(width: self.previewView.bounds.size.width, height: self.previewView.bounds.size.height)
                    assetWriter.prepareRecording()
                    assetWriter.setVideoOrientation(.portrait)
                    self.fileWriter = assetWriter
                    assetWriter.startWriting()
                }
//            // prepare output
//            let fileURL = URL(fileURLWithPath: copiedPath)
//            let assetWriter = SWRecordAssetWriter(type: "video", fileURL: fileURL)
//            assetWriter.addOutputs(to: session, queue: sessionQueue)
//            assetWriter.setVideoSize(width: self.previewView.bounds.size.width, height: self.previewView.bounds.size.height)
//            assetWriter.prepareRecording()
//            assetWriter.setVideoOrientation(.portrait)
//            self.fileWriter = assetWriter
//            assetWriter.startWriting()
        } else {
            // finish
            self.fileWriter?.stopWriting()
//            self.fileWriter = nil
            self.perform(#selector(test), with: nil, afterDelay: 2)
        }
    }
    
    
    func test() {
        self.fileWriter = nil
    }
}

extension UIDeviceOrientation {
    var videoOrientation: AVCaptureVideoOrientation? {
        switch self {
        case .portrait: return .portrait
        case .portraitUpsideDown: return .portraitUpsideDown
        case .landscapeLeft: return .landscapeRight
        case .landscapeRight: return .landscapeLeft
        default: return nil
        }
    }
}

extension UIInterfaceOrientation {
    var videoOrientation: AVCaptureVideoOrientation? {
        switch self {
        case .portrait: return .portrait
        case .portraitUpsideDown: return .portraitUpsideDown
        case .landscapeLeft: return .landscapeLeft
        case .landscapeRight: return .landscapeRight
        default: return nil
        }
    }
}
