//
//  SWRecordAssetWriter.swift
//  SWVideoRecorder
//
//  Created by CHOI-MS on 2016. 11. 15..
//  Copyright © 2016년 CHOI-MS. All rights reserved.
//

import Foundation
import AVFoundation
import Photos

class SWRecordAssetWriter: NSObject, AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureAudioDataOutputSampleBufferDelegate {
    
    // type will be replaced like [audio,video]
    init(type: String, fileURL: URL) {
//        do {
//            assetWriter = try AVAssetWriter(url: fileURL, fileType: AVFileTypeMPEG4)
//        }catch {
//            assetWriter = nil
//        }
        self.type = type
        self.fileURL = fileURL
    }
    
    private let writingQueue = DispatchQueue(label: "writing queue", attributes: [], target: nil)
    private let videoOutputQueue = DispatchQueue(label: "videoOutput queue", attributes: [], target: nil)
    private let audioOutputQueue = DispatchQueue(label: "audioOutput queue", attributes: [], target: nil)
    private var assetWriter: AVAssetWriter!
    private let fileURL: URL
    private var videoSourceFormat: CMFormatDescription? // sample 로 받은
    private var audioSourceFormat: CMFormatDescription?
    private var videoSettings: [AnyHashable : Any]?
    private var audioSettings: [AnyHashable : Any]?
    private var videoDataOutput: AVCaptureVideoDataOutput?
    private var audioDataOutput: AVCaptureAudioDataOutput?
    private var videoConnection: AVCaptureConnection?
    private var audioConnection: AVCaptureConnection?
    private var videoCompressionSettings: [String : Any]? // 파일에 저장할 포맷
    private var audioCompressionSettings:[String:Any]? // = [AVFormatIDKey : kAudioFormatMPEG4AAC] // 파일에 저장할 포맷
    private var videoAssetWriterInput: AVAssetWriterInput?
    private var audioAssetWriterInput: AVAssetWriterInput?
    private var startRecording = false {
        didSet {
            if self.startRecording == true {
                writingQueue.async { [unowned self] in
                    self.assetWriter?.startWriting()
                }
            } else {
                writingQueue.async { [unowned self] in
                    self.assetWriter?.finishWriting{
                        print("AssetWriter did Finish Recording")
                        self.copyFileToCameraRoll()
                    }
                }
            }
        }
    }
    private var assetStart = false
    
    private let type: String
    
    func addTrack(type: String, sourceFormat: CMFormatDescription, settings: [String:Any]) {
        if type == "video" {
            addVideoTrack(sourceFormat: sourceFormat, settings: settings)
        } else if type == "audio" {
            addAudioTrack(sourceFormat: sourceFormat, settings: settings)
        }
        // mschoi test
        addAudioTrack(sourceFormat: sourceFormat, settings: settings)
    }
    
    func addOutputs(to session: AVCaptureSession) {
        // contain 여부를 확인해야함.
        if type == "video" {
            addVideoDataOutput(to: session)
            videoConnection = videoDataOutput!.connection(withMediaType: AVMediaTypeVideo)
        }
        
        if type == "audio" {
            addAudioDataOutput(to: session)
            audioConnection = audioDataOutput!.connection(withMediaType: AVMediaTypeAudio)
            audioCompressionSettings = audioDataOutput!.recommendedAudioSettingsForAssetWriter(withOutputFileType: AVFileTypeAppleM4A) as? [String:Any]
        }
//         mschoi test
            addAudioDataOutput(to: session)
            audioConnection = audioDataOutput!.connection(withMediaType: AVMediaTypeAudio)
            audioCompressionSettings = audioDataOutput!.recommendedAudioSettingsForAssetWriter(withOutputFileType: AVFileTypeAppleM4A) as? [String:Any]
    }
    
    func setVideoOrientation(_ videoOrientation: AVCaptureVideoOrientation) {
        videoConnection?.videoOrientation = videoOrientation
    }
    
    func setVideoSize(width: CGFloat, height: CGFloat) {
        videoCompressionSettings = [AVVideoCodecKey : AVVideoCodecH264,
                                    AVVideoWidthKey : width,
                                    AVVideoHeightKey : height,
                                    AVVideoScalingModeKey : AVVideoScalingModeResizeAspectFill] // 이 값이 있으면 해당 크기만큼 짤리고, 이 값 빼면 찌그러진다.
    }
    
    func prepareRecording() {
        SWRecordAssetWriter.removeItem(at: fileURL) // 이미 이 경로에 아이템이 있으면 지워야하고, 해당 경로가 없을 수 있으므로 새로 만들어야한다.
        let dirPath = fileURL.absoluteString.components(separatedBy: "/").last!
        SWRecordAssetWriter.createDirectory(atPath: dirPath)
        
        self.assetStart = false // didSampleOutput 에서 컨트롤 하기 위함
        
        if assetWriter == nil {
            var fileType = "unknown"
            if type == "video" {
                fileType = AVFileTypeMPEG4
            } else if type == "audio" {
                fileType = AVFileTypeAppleM4A
            }
            
            do {
                assetWriter = try AVAssetWriter(url: fileURL, fileType: fileType)
            } catch {
            }
            
            addAssetInputs()
        }
    }
    
    func startWriting() {
        startRecording = true
    }
    
    func stopWriting() {
        startRecording = false
    }
    
    private func addAssetInputs() {
        if type == "video" {
            if assetWriter.canApply(outputSettings: videoCompressionSettings, forMediaType: AVMediaTypeVideo) {
                videoAssetWriterInput = AVAssetWriterInput(mediaType: AVMediaTypeVideo, outputSettings: videoCompressionSettings, sourceFormatHint: videoSourceFormat)
                videoAssetWriterInput!.expectsMediaDataInRealTime = true
                
                if assetWriter.canAdd(videoAssetWriterInput!) {
                    assetWriter.add(videoAssetWriterInput!)
                }
            }
        } else if type == "audio" {
            if assetWriter.canApply(outputSettings: audioCompressionSettings, forMediaType: AVMediaTypeAudio) {
                audioAssetWriterInput = AVAssetWriterInput(mediaType: AVMediaTypeAudio, outputSettings: audioCompressionSettings, sourceFormatHint: audioSourceFormat)
                audioAssetWriterInput!.expectsMediaDataInRealTime = true
                
                if assetWriter.canAdd(audioAssetWriterInput!) {
                    assetWriter.add(audioAssetWriterInput!)
                }
            }
        }
        
        // mschoi test
        if assetWriter.canApply(outputSettings: audioCompressionSettings, forMediaType: AVMediaTypeAudio) {
            audioAssetWriterInput = AVAssetWriterInput(mediaType: AVMediaTypeAudio, outputSettings: audioCompressionSettings, sourceFormatHint: audioSourceFormat)
            audioAssetWriterInput!.expectsMediaDataInRealTime = true
            
            if assetWriter.canAdd(audioAssetWriterInput!) {
                assetWriter.add(audioAssetWriterInput!)
            }
        }
    }
    
    private func addAudioTrack(sourceFormat: CMFormatDescription, settings: [String:Any]) {
        audioSourceFormat = sourceFormat
        audioSettings = settings
    }
    
    private func addVideoTrack(sourceFormat: CMFormatDescription, settings: [String:Any]) {
        videoSourceFormat = sourceFormat
        videoSettings = settings
    }
    
    private func addVideoDataOutput(to session: AVCaptureSession) {
        videoDataOutput = AVCaptureVideoDataOutput()
        videoDataOutput!.videoSettings = videoSettings
        videoDataOutput!.alwaysDiscardsLateVideoFrames = false
        videoDataOutput!.setSampleBufferDelegate(self, queue: videoOutputQueue)
        
        addOuput(videoDataOutput!, to: session)
    }
    
    private func addAudioDataOutput(to session: AVCaptureSession) {
        audioDataOutput = AVCaptureAudioDataOutput()
        audioDataOutput!.setSampleBufferDelegate(self, queue: audioOutputQueue)
        
        addOuput(audioDataOutput!, to: session)
    }
    
    private func addOuput(_ output: AVCaptureOutput, to session: AVCaptureSession) {
        
        if session.canAddOutput(output) {
            session.addOutput(output)
        }
    }
    
    // MARK: - preparing
    private class func removeItem(at url: URL) {
        do {
            try FileManager.default.removeItem(at: url)
        } catch {
        }
    }
    
    private class func createDirectory(atPath: String) {
        do {
            try FileManager.default.createDirectory(atPath: atPath, withIntermediateDirectories: true)
        } catch {
        }
    }
    
    // DataOutputSampleBufferDelegate - 여기는 레이어를 통해 계속 호출이 된다.
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, from connection: AVCaptureConnection!) {
        
        if startRecording {
            writingQueue.async { [unowned self] in
                
                if self.assetStart == false && self.assetWriter.status == .writing {
                    print("assetStatus = \(self.assetWriter.status.rawValue)")
                    self.assetStart = true
                    let sourceTime = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)
                                        
                    self.assetWriter.startSession(atSourceTime: sourceTime)
                    print("AssetWriter did Start Recording")
                }
                
                if self.assetWriter.status == .writing {
                    let formatDescription = CMSampleBufferGetFormatDescription(sampleBuffer)
                    if connection == self.videoConnection {
                        //                    self.videoAssetWriterInput?.append(sampleBuffer)
                        
                        if (self.videoAssetWriterInput?.isReadyForMoreMediaData)! {
                            let success = self.videoAssetWriterInput?.append(sampleBuffer)
                            
                            if success == false {
                                print("asdfasdfasdfasdf" )
                            }
                        }
                        
                    } else if connection == self.audioConnection {
                        self.audioSourceFormat = formatDescription
                        self.audioAssetWriterInput?.append(sampleBuffer)
                    }
                }
                
//                let formatDescription = CMSampleBufferGetFormatDescription(sampleBuffer)
//                if connection == self.videoConnection {
////                    self.videoAssetWriterInput?.append(sampleBuffer)
//                    
//                    if self.assetStart == false {
//                        self.assetStart = true
//                        let sourceTime = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)
//                        self.assetWriter.startSession(atSourceTime: sourceTime)
//                    }
//                    
//                    if (self.videoAssetWriterInput?.isReadyForMoreMediaData)! {
//                        let success = self.videoAssetWriterInput?.append(sampleBuffer)
//                        
//                        if success == false {
//                            print("asdfasdfasdfasdf" )
//                        }
//                    }
//                    
//                } else if connection == self.audioConnection {
//                    self.audioSourceFormat = formatDescription
//                    self.audioAssetWriterInput?.append(sampleBuffer)
//                }
            }
        }
    }
    
    private func copyFileToCameraRoll() {
        
        print("hoho")
        
        PHPhotoLibrary.requestAuthorization {[unowned self]  status in
            if status == .authorized {
                // Save the movie file to the photo library and cleanup.
                PHPhotoLibrary.shared().performChanges({
                    let options = PHAssetResourceCreationOptions()
                    options.shouldMoveFile = true
                    let creationRequest = PHAssetCreationRequest.forAsset()
                    print("fileURL = \(self.fileURL)")
                    creationRequest.addResource(with: .video, fileURL: self.fileURL, options: options)
                }, completionHandler: { success, error in
                    if !success {
                        print("Could not save movie to photo library: \(error)")
                    } else {
                        print("success")
                    }
                }
                )
            }
            else {
                print("photo access fail")
            }
        }
    }
}
